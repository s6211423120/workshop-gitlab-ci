ROOT_DIR := $(dir $(realpath $(lastword $(MAKEFILE_LIST))))

# Start PostgreSQL Server on port 54321
pgu:
	docker run \
	--name demo-db \
	-p 54321:5432 \
	-e POSTGRES_PASSWORD=pgpwd \
	-e POSTGRES_DB=demo-db \
	-v pg-demo-db:/var/lib/postgresql/data \
	-d postgres:15-alpine

# Stop PostgreSQL Server
pgd:
	docker stop demo-db

# Remove PostgreSQL Server and volume data
pgrm:
	docker rm demo-db &&	\
	docker volume rm pg-demo-db

# Create migration file
# make mgc filename=YOUR_FILE_NAME
mgc:
	docker run --rm -v $(ROOT_DIR)migrations:/migrations migrate/migrate:v4.15.2 -verbose create -ext sql -dir /migrations $(filename)

# Run migration up to latest version
mgu:
	docker run --rm --network host -v $(ROOT_DIR)migrations:/migrations migrate/migrate:v4.15.2 -verbose -path=/migrations/ -database "postgresql://postgres:pgpwd@localhost:54321/demo-db?sslmode=disable" up

# Run migration down by 1 version
mgd:
	docker run --rm --network host -v $(ROOT_DIR)migrations:/migrations migrate/migrate:v4.15.2 -verbose -path=/migrations/ -database "postgresql://postgres:pgpwd@localhost:54321/demo-db?sslmode=disable" down 1

build:
	docker image build -t registry.gitlab.com/somprasongd/gitlab-ci-cd .